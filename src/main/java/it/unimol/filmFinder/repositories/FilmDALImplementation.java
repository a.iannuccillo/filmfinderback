package it.unimol.filmFinder.repositories;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import it.unimol.filmFinder.models.Film;

@Repository
public class FilmDALImplementation implements FilmDAL {
	
	private final MongoTemplate mongoTemplate;

	@Autowired
	public FilmDALImplementation(MongoTemplate mongoTemplate) {
		this.mongoTemplate= mongoTemplate;
	}

	@Override
	public Film newFilm(Film film) {
		mongoTemplate.save(film);
		return film;
	}

	@Override
	public List<Film> getAllFilm() {
		return mongoTemplate.findAll(Film.class);
	}

	@Override
	public List<Film> findFilm(String mood,boolean answer) {
		List<Film> film;
		if(mood.startsWith("ben")|| mood.startsWith("mal")) {
			if(answer) {
				film = findFilmByMood("Relax");
			}else {
				film = findFilmByMood("Piangere");
			}
		
		}else {
			film = findFilmByMood("NoStress");
		}
		
		return film;
	}

	@Override
	public List<Film> findFilmByMood(String mood) {
		Query query = new Query();
		query.addCriteria(Criteria.where("mood").is(mood));
		
		return mongoTemplate.find(query,Film.class,"film");
	}
	
	
	@Override
	public Film updateOne(ObjectId _id, String field, String data) {
		// TODO Auto-generated method stub
		return null;
	}


	

}
