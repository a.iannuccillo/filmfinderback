package it.unimol.filmFinder.models.utils;

public class Body {
	
	public String mood;
	public boolean answer;
	
	public String getMood() {
		return mood;
	}
	public void setMood(String field) {
		this.mood = field;
	}
	public boolean getAnswer() {
		return answer;
	}
	public void setAnswer(boolean data) {
		this.answer = data;
	}
	
}
