package it.unimol.filmFinder.repositories;

import java.util.List;

import org.bson.types.ObjectId;

import it.unimol.filmFinder.models.Film;

public interface FilmDAL {
	
	Film newFilm(Film film);
	List<Film> getAllFilm();
	List<Film> findFilm(String mood, boolean answer);
	List<Film> findFilmByMood(String mood);
	Film updateOne(ObjectId _id, String field, String data);

}
