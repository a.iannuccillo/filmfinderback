package it.unimol.filmFinder.models;

public class Status {
	public int code;
	public String status;
	
	public Object result;

	public Status() {}
	public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

}
