package it.unimol.filmFinder.models;

public class Film {
	
	public String name;
	public String image;
	public String[] filmGenre;
	public String country;
	public String releaseDate;
	private String[] mood;
	public String[] streaming;
	public String plot;
	public int runningTime;
	
	
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getFilmGenre() {
		return filmGenre;
	}
	public void setFilmGenre(String[] filmGenre) {
		this.filmGenre = filmGenre;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String[] getMood() {
		return mood;
	}
	public void setMood(String[] mood) {
		this.mood = mood;
	}
	public String[] getStreaming() {
		return streaming;
	}
	public void setStreaming(String[] streaming) {
		this.streaming = streaming;
	}
	public String getPlot() {
		return plot;
	}
	public void setPlot(String plot) {
		this.plot = plot;
	}
	public int getRunningTime() {
		return runningTime;
	}
	public void setRunningTime(int runningTime) {
		this.runningTime = runningTime;
	}
	
	
}
