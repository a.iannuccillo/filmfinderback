package it.unimol.filmFinder.models;

public class TypeOfGenre {
	
	public String name;
	public boolean isGenre;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isGenre() {
		return isGenre;
	}
	public void setIsGenre(boolean yes) {
		this.isGenre = yes;
	}
	
	
}
