package it.unimol.filmFinder.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import it.unimol.filmFinder.models.Film;
import it.unimol.filmFinder.models.Status;
import it.unimol.filmFinder.models.utils.Body;
import it.unimol.filmFinder.repositories.FilmDAL;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;

@Controller
@RequestMapping("/api/film")
public class FilmController {

	@Autowired
	private FilmDAL repositoryFilm;

	public Status responseStatus = new Status();

	@RequestMapping(value = "/ciao", method = RequestMethod.GET)
	public String film() {
		return "ciao";
	}

	@RequestMapping(value = "/newFilm", method = RequestMethod.POST)
	public ResponseEntity<?> createFilm(@Valid @RequestBody Film body) {

		try {

			Film film = repositoryFilm.newFilm(body);

			responseStatus.setStatus("Ristorante aggiunto con successo");
			responseStatus.setCode(0);
			responseStatus.setResult(film);

		} catch (Exception e) {
			responseStatus.setCode(-1);
			responseStatus.setStatus("Ops qualcosa è andato storto");
			responseStatus.setResult(null);

		}
		return ResponseEntity.ok(responseStatus);
	}
	@CrossOrigin()
	@RequestMapping(value = "/findFilmByMood", method = RequestMethod.POST)
	public ResponseEntity<?> findFilmByMood(@Valid @RequestBody Body body) {

		try {

			List<Film> film = repositoryFilm.findFilm(body.getMood(),body.getAnswer());

			if (film.size() != 0) {
				responseStatus.setStatus("Film trovati con successo");
				responseStatus.setCode(0);
				responseStatus.setResult(film);
			}else {
				responseStatus.setStatus("Film non trovati");
				responseStatus.setCode(-1);
				responseStatus.setResult("");
			}

		} catch (Exception e) {
			responseStatus.setCode(-2);
			responseStatus.setStatus("Ops qualcosa è andato storto");
			responseStatus.setResult("");

		}
		return ResponseEntity.ok(responseStatus);
	}

}
